var express = require('express');
var router = express.Router();
var _ = require('lodash-node');

var types = [
    'Family',
    'Non-Profit',
    'Business',
    'Club',
    'Team',
    'Other'
];

//router.use('/:id/members', require('./groups.members'));

router.get('/types', function (req, res) {
    res.send(types);
});

router.get('/:id', function (req, res) {
    var groups = req.db.get('groups');
    groups.findById(req.params.id)
        .then(function (model) {
            if (model) {
                res.send(model);
            }
            else {
                res.status(404).send();
            }
        })
        .catch(function (err) {
            req.logger.error(err);
            res.status(500).send(err);
        });
});

router.get('/:id/detailed', function (req, res) {
    var groups = req.db.get('groups');
    var users = req.db.get('users');
    groups.findById(req.params.id)
        .then(function (group) {
            var userArray = _.map(group.members, function (member) {
                return users.id(member.userId);
            });
            users.find({_id: {$in: userArray}})
                .then(function (users) {
                    _.each(group.members, function (member) {
                        member.user = _.find(users, function (user) {
                            return user._id == member.userId;
                        });
                    });
                    res.send(group);
                })
                .catch(function (err) {
                    req.logger.error(err);
                    res.status(500).send(err);
                })
        })
        .catch(function (err) {
            req.logger.error(err);
            res.status(500).send(err);
        });
});

router.post('/', function (req, res) {
    var groups = req.db.get('groups');
    req.body.members = [
        {
            userId: req.principle.user.id,
            admin: true,
            joined: new Date()
        }
    ];
    groups.insert(req.body)
        .then(function (group) {
            res.status(201).location('/api/groups/' + group._id).send();
        })
        .catch(function (err) {
            req.logger.error(err);
            res.status(500).send(err);
        });
});

router.put('/', function (req, res) {
    var groups = req.db.get('groups');
    groups.updateById(req.body._id, req.body)
        .then(function (result) {
            res.status(200).send();
        })
        .catch(function (err) {
            res.status(500).send(err);
        });
});

router.delete('/:id', function (req, res) {
    var groups = req.db.get('groups');
    groups.remove({_id: req.params.id})
        .then(function () {
            res.status(200).send();
        })
        .catch(function (err) {
            res.status(500).send(err);
        });

});

module.exports = router;
