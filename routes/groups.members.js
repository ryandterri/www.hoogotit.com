var router = require('express').Router({mergeParams: true});
var groupMembers = require('../controllers/crud.server.controller')('groupMembers');
var users = require('../controllers/crud.server.controller')('users');
var _ = require('lodash-node');

router.get('/', function (req, res) {
    var groups = req.db.get('groups');
    var users = req.db.get('users');
    groups.findById(req.params.id)
        .then(function (group) {
            var userArray = _.map(group.members, function (member) {
                return users.id(member.userId);
            });
            users.find({_id: {$in: userArray}})
                .then(function (users) {
                    _.each(group.members, function (member) {
                        member.user = _.find(users, function (user) {
                            return user._id == member.userId;
                        });
                    });
                    res.send(group);
                })
                .catch(function (err) {
                    req.logger.error(err);
                    res.status(500).send(err);
                })
        })
        .catch(function (err) {
            req.logger.error(err);
            res.status(500).send(err);
        });
});

router.put('/', function (req, res) {
    req.body.groupId = req.params.id;
    groupMembers.update(req.body)
        .then(function (member) {
            res.send(member);
        })
        .catch(function (err) {
            res.status(err.statusCode).send();
        });
});

router.post('/', function (req, res) {
    req.body.groupId = req.params.id;
    groupMembers.insert(req.body)
        .then(function (member) {
            res.send(member);
        })
        .catch(function (err) {
            res.status(err.statusCode).send();
        });
});

module.exports = router;