var router = require('express').Router();
var utils = require('../utils');

router.use(utils.authenticated);

router.use('/tasks', require('./tasks'));
router.use('/groups', require('./groups'));
router.use('/groupMembers', require('./groups.members'));
router.use('/users', require('./users'));

module.exports = router;