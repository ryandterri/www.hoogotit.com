var express = require('express');
var _ = require('lodash-node');
var router = express.Router();
var utils = require('../utils');

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index');
});

router.get('/login', function (req, res) {
    res.render('login');
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/login');
});

module.exports = router;
