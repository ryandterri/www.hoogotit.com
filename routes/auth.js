var express = require('express');
var router = express.Router();
var utils = require('../utils');
var config = require('../config');
var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var jwt = require('jsonwebtoken');
var users = require('../controllers/crud.server.controller')('users');

passport.use(new GoogleStrategy({
        clientID: config.auth.google.GOOGLE_CLIENT_ID,
        clientSecret: config.auth.google.GOOGLE_CLIENT_SECRET,
        callbackURL: config.auth.google.GOOGLE_CALLBACK_URL
    },
    function (accessToken, refreshToken, profile, done) {
        var email = profile.emails[0].value;
        var picture = profile.photos[0].value;

        process.nextTick(function () {
            users.get({email: email})
                .then(function (user) {
                    if (user) {
                        user.accessToken = accessToken;
                        user.refreshToken = refreshToken;
                        return done(null, user);
                    }
                    else {
                        var newUser = {
                            username: email,
                            email: email,
                            picture: picture,
                            displayName: profile.displayName
                        };
                        users.insert(newUser)
                            .then(function (added) {
                                newUser.accessToken = accessToken;
                                newUser.refreshToken = refreshToken;
                                return done(null, newUser);
                            })
                            .catch(function (err) {
                                console.log(err);
                            });
                    }
                })
                .catch(function (err) {
                    return done(err, null);
                });
        });
    }
));

router.get('/google', passport.authenticate('google', {
    scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.google.com/m8/feeds']
}));

router.get('/google/callback', function (req, res, next) {
    passport.authenticate('google', function (err, user, info) {
        if (!user) {
            return res.redirect('/#/login');
        }

        var userData = {
            name: user.displayName,
            email: user.email,
            id: user._id,
            picture: user.picture,
            accessToken: user.accessToken,
            refreshToken: user.refreshToken
        };
        var rolesData = {};
        if (user.roles) {
            user.roles.forEach(function (r) {
                rolesData[r] = true;
            });
        }

        var tokenData = {
            user: userData,
            roles: rolesData
        };

        var token = jwt.sign(tokenData,
            config.auth.token.secret,
            {expiresIn: config.auth.token.expiresInMinutes * 60});

        res.cookie(config.auth.cookieName, token);
        res.redirect('/#/profile');
    })(req, res.next);
});

module.exports = router;
