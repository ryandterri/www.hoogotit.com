var express = require('express');
var router = express.Router();
var utils = require('../utils');

/* GET home page. */
router.get('/', function(req, res) {

    utils.db.tasks.getAll(req.user, function(err, tasks){
        if (err)
            res.send('Error finding tasks to get');
        else {
            //res.send(tasks);
            res.render('tasks/tasks', {
                title: 'My Tasks',
                tasks: tasks,
                user: req.user
            });
        }
    });
});

router.get('/get/:id', function(req, res){
   utils.db.tasks.assignToUser(req.user.id, parseInt(req.params.id), function(err){
        if (err)
            res.send(err);
        else {
            res.redirect('/');
        }
   });
});

router.get('/complete/:id', function(req, res){
    utils.db.tasks.setComplete(req.user.id, parseInt(req.params.id), function(err){
        if (err)
            res.send(err);
        else {
            res.redirect('/');
        }
    });
});

router.get('/tocomplete', utils.authenticated, function(req, res) {
    utils.db.users.getAssignedTasks(req.user, function(err, tasks){
        if (err)
            res.send('Error finding tasks to get');
        else {
            res.render('tasks/taskstocomplete', {
                title: 'My Tasks to complete',
                tasks: tasks,
                user: req.user
            });
        }
    });
});

router.get('/toconfirm', utils.authenticated, function(req, res) {

    utils.db.users.getCompleteTasks(req.user, function(err, tasks){
        if (err)
            res.send('Error finding tasks to get');
        else {
            res.render('tasks/taskstoconfirm', {
                title: 'My Tasks to confirm',
                tasks: tasks,
                user: req.user
            });
        }
    });
});

router.get('/toget', utils.authenticated, function(req, res) {
    utils.db.users.getAvailableTasks(req.user, function(err, tasks){
        if (err)
            res.send('Error finding tasks to get');
        else {
            res.render('tasks/taskstoget', {
                title: 'My Tasks to get',
                tasks: tasks,
                user: req.user
            });
        }
    });
});

module.exports = router;
