var express = require('express');
var router = express.Router();
var config = require('../config');
var client = require('gdata-js')(config.auth.google.GOOGLE_CLIENT_ID, config.auth.google.GOOGLE_CLIENT_SECRET);
var _ = require('lodash-node');

router.get('/me', function (req, res) {
    res.send(req.principle.user);
});

router.get('/me/groups', function (req, res) {
    var groups = req.db.get('groups');
    groups.find({members: {$elemMatch: {userId: req.principle.user.id}}})
        .then(function (groups) {
            res.send(groups);
        })
        .catch(function (err) {
            res.status(500).send(err);
        });
});

/* GET users listing. */
router.get('/google', function (req, res) {

    var user = req.principle.user;

    console.log('Getting Contacts');
    console.log('Access Token: ' + user.accessToken);
    console.log('Refresh Token: ' + user.refreshToken);
    client.setToken({access_token: user.accessToken, refresh_token: user.refreshToken});

    client.getFeed('https://www.google.com/m8/feeds/contacts/default/full', {'max-results': 200}, function (err, result) {
        if (err) {
            console.log(err);
        }
        else {
            for (var i = 0; i < result.feed.entry.length; i++) {
                console.log(result.feed.entry[i]);
            }
            res.send(result.feed.entry);
        }
    });
});

module.exports = router;
