var config = require('../config');
var monk = require('monk');
var db = monk(config.mongo.MONGO_URL);
var log4js = require('log4js');
var mongoAppender = require('log4js-node-mongodb');
var q = require('q');
log4js.addAppender(
    mongoAppender.appender({connectionString: config.mongo.MONGO_URL}),
    'logger'
);
var logger = log4js.getLogger('logger');


module.exports = function (collectionName) {
    var collection = db.get(collectionName);
    return {
        id: function (id) {
            return collection.id(id);
        },
        get: function (query) {
            var deferred = q.defer();
            collection.findOne(query, function (err, model) {
                if (err) {
                    logger.error(err);
                    err.statusCode = 500;
                    deferred.reject(err);
                }
                else if (!model) {
                    var e = new Error('Could not find ' + collectionName + ' with id: ' + id);
                    logger.info(e);
                    e.statusCode = 404;
                    deferred.reject(e);
                }
                else {
                    deferred.resolve(model);
                }
            });
            return deferred.promise;
        },
        find: function (query) {
            var deferred = q.defer();
            collection.find(query, function (err, models) {
                if (err) {
                    logger.error(err);
                    err.statusCode = 500;
                    deferred.reject(err);
                }
                else if (!models) {
                    var e = new Error('Could not find any ' + collection6Name + ' with search options: ' + query);
                    logger.info(e);
                    e.statusCode = 404;
                    deferred.reject(e);
                }
                else {
                    deferred.resolve(models);
                }
            });
            return deferred.promise;
        },
        insert: function (model) {
            var deferred = q.defer();
            collection.insert(model, function (err, model) {
                if (err) {
                    logger.error(err);
                    err.status = 500;
                    deferred.reject(err);
                }
                else {
                    deferred.resolve(model);
                }
            });
            return deferred.promise;
        },
        update: function (model) {
            var deferred = q.defer();
            collection.updateById(model._id, model, function (err, model) {
                if (err) {
                    logger.error(err);
                    err.status = 500;
                    deferred.reject(err);
                }
                else {
                    deferred.resolve(model);
                }
            });
            return deferred.promise;
        },
        delete: function (query) {
            var deferred = q.defer();
            collection.remove(query, function (err, result) {
                if (err) {
                    var e = new Error('Could not find any ' + collection6Name + ' with id: ' + id);
                    logger.error(err);
                    e.status(404);
                    deferred.reject(e);
                }
                else {
                    deferred.resolve(true);
                }
            });
            return deferred.promise;
        }
    }
}