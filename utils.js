var _ = require('lodash-node');
var config = require('./config');
var monk = require('monk');
var db = monk(config.mongo.MONGO_URL);
var jwt = require('jsonwebtoken');
var log4js = require('log4js');
var mongoAppender = require('log4js-node-mongodb');
log4js.addAppender(
    mongoAppender.appender({connectionString: config.mongo.MONGO_URL}),
    'logger'
);
var logger = log4js.getLogger('logger');

var utils = {
        authenticated: function (req, res, next) {
            req.db = db;
            req.logger = logger;
            var token = req.headers['x-access-token'];

            if (token) {
                try {
                    var decoded = jwt.verify(token, config.auth.token.secret);

                    req.principle = {
                        isAuthenticated: true,
                        roles: decoded.roles || [],
                        user: decoded.user
                    };
                    return next();
                }
                catch (err) {
                    console.log('Error parsing access token', err);
                }
            }

            return res.status(401).json({error: 'Invalid access token!'});
        }
    };

module.exports = utils;