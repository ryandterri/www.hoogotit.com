/**
 * Created by Purcell on 6/13/2016.
 */
angular.module('hoogotit', [
      'ui.router'
    , 'ui.bootstrap'
    , 'hoogotit.service.api'
    , 'angular-jwt'
    , 'ngCookies'
])
    .config(['$urlRouterProvider', function ($urlRouterProvider) {
        $urlRouterProvider.otherwise("/login");
    }]);
