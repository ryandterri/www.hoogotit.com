/**
 * Created by JJ on 6/23/2016.
 */
angular.module('hoogotit.service.api')
    .factory('UserService', ['ApiServiceCore', function (ApiServiceCore) {

        return {
            users: {
                me: function () {
                    return ApiServiceCore.processRequest({
                        url: '/api/users/me',
                        method: 'GET'
                    });
                }
            }
        };
    }]);


