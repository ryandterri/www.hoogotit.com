/**
 * Created by JJ on 6/23/2016.
 */
angular.module('hoogotit.service.api')
    .factory('GroupService', ['ApiServiceCore', function (ApiServiceCore) {

        return {
            groups: {
                myGroups: function () {
                    return ApiServiceCore.processRequest({
                        url: '/api/users/me/groups',
                        method: 'GET'
                    });
                },
                get: function (id) {
                    return ApiServiceCore.processRequest({
                        url: '/api/groups/' + id,
                        method: 'GET'
                    });
                },
                getDetailed: function (id) {
                    return ApiServiceCore.processRequest({
                        url: '/api/groups/' + id + '/detailed',
                        method: 'GET'
                    });
                },
                update: function (group) {
                    return ApiServiceCore.processRequest({
                        url: '/api/groups/',
                        method: 'PUT',
                        data: group
                    });
                },
                add: function (group) {
                    return ApiServiceCore.processRequest({
                        url: '/api/groups',
                        method: 'POST',
                        data: group
                    });
                },
                delete: function (id) {
                    return ApiServiceCore.processRequest({
                        url: '/api/groups/' + id,
                        method: 'DELETE'
                    });
                },
                types: {
                    all: function () {
                        return ApiServiceCore.processRequest({
                            url: '/api/groups/types',
                            method: 'GET'
                        });
                    }
                },
                members: {
                    all: function (groupId) {
                        return ApiServiceCore.processRequest({
                            url: '/api/groups/' + groupId + '/members',
                            method: 'GET'
                        });
                    },
                    add: function (groupMember) {
                        return ApiServiceCore.processRequest({
                            url: '/api/groups/' + groupMember.groupId + '/members',
                            method: 'POST',
                            data: groupMember
                        });
                    },
                    update: function (groupMember) {
                        return ApiServiceCore.processRequest({
                            url: '/api/groups/' + groupMember.groupId + '/members',
                            method: 'PUT',
                            data: groupMember
                        });
                    }
                }
            }
        };
    }]);


