angular.module('hoogotit.service.api', ['ui.router', 'hoogotit.service.principle'])
    .factory('ApiServiceCore', ['$http', '$state', 'principle', function ($http, $state, principle) {

        return {
            processRequest: function (req) {
                angular.extend(req, {headers: {'x-access-token': principle().token}});
                return $http(req).then(function (response) {
                    return response.data;
                }, function (error) {
                    if (error.status === 401) {
                        $state.go('login');
                    }
                    else{
                        //$log.debug('Bad error happened');
                    }

                });
            }
        };
    }]);