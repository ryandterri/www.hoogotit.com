angular.module('hoogotit.service.principle', [])
    .factory('principle', function($cookies, jwtHelper){
        return function () {
            var principal = { isAuthenticated: false, roles: [], user: { name: 'Guest' } };

            try{
                var token = $cookies.get('_accessToken');
                var decoded = jwtHelper.decodeToken(token);

                if(decoded && !jwtHelper.isTokenExpired(token)){

                    principal.isAuthenticated = true;
                    principal.roles = decoded.roles;
                    principal.user = decoded.user;
                    principal.token = token;
                }
            }
            catch(e){
                console.log('ERROR while parsing principal cookie.');
            }

            principal.logout = function(){
                $cookies.remove('_accessToken');
            };

            return principal;    
        };
    });
