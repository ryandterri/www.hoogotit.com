/**
 * Created by JJ on 6/21/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.tasks', {
        url: '/tasks',
        template: '<div ui-view></div>',
        abstract: true
    });
}]);