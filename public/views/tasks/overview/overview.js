/**
 * Created by JJ on 6/21/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.tasks.overview', {
        url: '/tasks/overview',
        templateUrl: '/views/tasks/overview/overview.html',
        controller: [function () {
            //var ctrl = this;

        }],
        controllerAs: 'ctrl'
    });
}]);