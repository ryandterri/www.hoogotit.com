/**
 * Created by JJ on 6/14/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.profile',
        {
            url: '/profile',
            templateUrl: '/views/profile/profile.html',
            resolve: {
                user: ['UserService', function (UserService) {
                    return UserService.users.me();
                }]
            },
            controller: ['user', function (user) {
                var ctrl = this;
                ctrl.user = user;
            }],
            controllerAs: 'ctrl'
        });
}]);