/**
 * Created by JJ on 6/19/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.groups.edit',
        {
            url: '/:id/edit',
            templateUrl: '/views/groups/edit/edit.html',
            resolve: {
                types: ['GroupService', function (GroupService) {
                    return GroupService.groups.types.all();
                }],
                group: ['GroupService', '$stateParams', function (GroupService, $stateParams) {
                    return GroupService.groups.get($stateParams.id);
                }]
            },
            controller: ['GroupService', '$state', 'types', 'group', function (GroupService, $state, types, group) {
                var ctrl = this;
                ctrl.group = group;
                ctrl.types = types;

                ctrl.save = function(){
                    GroupService.groups.update(ctrl.group).then(function (){
                        $state.go('master.groups.list', {}, {reload: true});
                    });
                };
            }],
            controllerAs: 'ctrl'
        });
}]);