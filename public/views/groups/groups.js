/**
 * Created by JJ on 6/14/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.groups',
        {
            url: '/groups',
            template: '<div ui-view></div>',
            abstract: true
        });
}]);