/**
 * Created by JJ on 6/21/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.groups.members',
        {
            url: '/:id/members',
            templateUrl: '/views/groups/members/members.html',
            resolve: {
                group: ['GroupService', '$stateParams', function (GroupService, $stateParams) {
                    return GroupService.groups.getDetailed($stateParams.id);
                }]
            },
            controller: ['GroupService', '$stateParams', 'group', function (GroupService, $stateParams, group) {
                var ctrl = this;

                ctrl.group = group;
              
                ctrl.toggleAdmin = function () {
                    var group = angular.copy(ctrl.group);
                    _.each(group.members, function(member){
                        delete member.user;
                    });

                    GroupService.groups.update(group, function() {
                        
                    });
                };

            }],
            controllerAs: 'ctrl'
        });
}]);