/**
 * Created by JJ on 6/20/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.groups.add',
        {
            url: '/add',
            templateUrl: '/views/groups/add/add.html',
            resolve: {
                types: ['GroupService', function (GroupService) {
                    return GroupService.groups.types.all();
                }]
            },
            controller: ['GroupService', '$state', 'types', function (GroupService, $state, types) {
                var ctrl = this;
                ctrl.newGroup = {};
                ctrl.types = types;

                ctrl.submit = function(){
                    GroupService.groups.add(ctrl.newGroup).then(function(){
                        $state.go('master.groups.list', {}, {reload: true});
                    });
                };
            }],
            controllerAs: 'ctrl'
        });
}]);