/**
 * Created by JJ on 6/20/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.groups.list',
        {
            url: '/list',
            templateUrl: '/views/groups/list/list.html',
            resolve: {
                groups: ['GroupService', function (GroupService) {
                    return GroupService.groups.myGroups();
                }]
            },
            controller: ['GroupService', 'groups', function (GroupService, groups) {
                var ctrl = this;

                ctrl.groups = groups;
                
                ctrl.delete = function (id) {
                    GroupService.groups.delete(id).then(function () {
                        ctrl.groups = _.filter(ctrl.groups, function (group) {
                            return group._id !== id;
                        });
                    });
                };
            }],
            controllerAs: 'ctrl'
        });
}]);