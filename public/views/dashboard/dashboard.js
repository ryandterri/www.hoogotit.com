/**
 * Created by JJ on 6/14/2016.
 */

angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.dashboard',
        {
            url: '/dashboard',
            templateUrl: '/views/dashboard/dashboard.html',
            resolve: {
                groups: ['GroupService', function (GroupService) {
                    return GroupService.groups.myGroups.then(function (response) {
                        return response.data;
                    });
                }]
            },
            controller: [function () {
                var ctrl = this;
                ctrl.test = "test";
            }],
            controllerAs: 'ctrl'
        }
    );
}]);