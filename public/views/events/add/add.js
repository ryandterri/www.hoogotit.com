/**
 * Created by JJ on 6/22/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.events.add',
        {
            url: '/add',
            params: {
                groupId: null,
            },
            resolve: {
                initialGroup: ['GroupService', '$stateParams', function (GroupService, $stateParams) {
                    if ($stateParams.groupId) {
                        return GroupService.groups.get($stateParams.groupId);
                    }
                    else
                        return null;
                }],
                userGroups: ['GroupService', function (GroupService) {
                    return GroupService.groups.myGroups();
                }]
            },
            templateUrl: '/views/events/add/add.html',
            controller: ['$stateParams', 'initialGroup', 'userGroups', function ($stateParams, initialGroup, userGroups) {
                var ctrl = this;

                ctrl.userGroups = userGroups;

                ctrl.newEvent = {};
                ctrl.newEvent.groupId = initialGroup;
            }],
            controllerAs: 'ctrl'
        });
}]);