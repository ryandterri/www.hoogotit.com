/**
 * Created by JJ on 6/22/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.events', {
        url: '/events',
        template: '<div ui-view></div>',
        abstract: true
    });
}]);