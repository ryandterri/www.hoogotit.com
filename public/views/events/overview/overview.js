/**
 * Created by JJ on 6/22/2016.
 */
angular.module('hoogotit').config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.events.overview', {
        url: '/overview',
        params: {
            groupId: null
        },
        templateUrl: '/views/events/overview/overview.html',
        controller: [function () {
            //var ctrl = this;
        }],
        controllerAs: 'ctrl'
    });
}]);