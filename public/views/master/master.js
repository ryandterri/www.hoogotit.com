/**
 * Created by Purcell on 6/13/2016.
 */
angular.module('hoogotit')
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('master', {
                templateUrl: 'views/master/master.html',
                controller: ['principle', '$state', function (principle, $state) {
                    var ctrl = this;
                    ctrl.user = principle().user;
                    ctrl.logout = function(){
                        principle().logout();
                        $state.go('login');
                    };
                }],
                controllerAs: 'ctrl'
            });
    }]);