/**
 * Created by ryan.purcell on 9/9/2014.
 */
module.exports = function (grunt) {

    grunt.initConfig({
        concat: {
            core: {
                src: [
                      'bower_components/jquery/dist/jquery.js'
                    , 'bower_components/bootstrap/dist/js/bootstrap.js'
                    , 'bower_components/angular/angular.js'
                    , 'bower_components/angular-ui-router/release/angular-ui-router.js'
                    , 'bower_components/underscore/underscore.js'
                    , 'bower_components/angular-bootstrap/ui-bootstrap-tpls.js'
                    , 'bower_components/angular-jwt/dist/angular-jwt.js'
                    , 'bower_components/angular-cookies/angular-cookies.js'
                ],
                dest: 'public/javascripts/site.js'
            },
            angular: {
                src: [
                      'public/app.js'
                    , 'public/views/**/*.js'
                    , 'public/services/**/*.js'
                ],
                dest: 'public/ng-hoogotit.js'
            }
        },
        less: {
            development: {
                files: {
                    'src/css/bootstrap.css': 'src/less/bootstrap.less',
                    'src/css/font-awesome.css': 'bower_components/font-awesome/less/font-awesome.less'
                }
            }
        },
        uglify: {
            development: {
                files: {
                    'public/javascripts/site.min.js': ['public/javascripts/site.js']
                }
            }
        },
        cssmin: {
            development: {
                files: {
                    'public/stylesheets/site.min.css': ['src/css/*.css']
                }
            }
        },
        jshint:{
            options:{
                laxcomma: true,
                eqeqeq: true,
                esversion: 5,
                undef: true,
                unused: true,
                predef: ['angular', 'console', '_']
            },
            all: ['public/app.js'
                , 'public/views/**/*.js'
                , 'public/services/**/*.js']
        },
        watch: {
            uglify: {
                files: 'public/javascripts/site.js',
                tasks: 'uglify'
            },
            cssmin: {
                files: 'src/css/*.css',
                tasks: 'cssmin',
                options: {
                    livereload: true
                }
            },
            less: {
                files: 'src/less/*.less',
                tasks: 'less'
            },
            concat: {
                files: ['public/app.js', 'public/views/**/*.js', 'public/services/**/*.js'],
                tasks: 'angular-build'
            }
        }
    });

    require('load-grunt-tasks')(grunt);
    grunt.registerTask('build', ['concat', 'uglify', 'less', 'cssmin']);
    grunt.registerTask('angular-build', ['jshint', 'concat:angular']);
};